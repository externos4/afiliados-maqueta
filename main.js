function sliding(){
    let counter = 0;
    let timeSlide = 5000;

    let slides = document.querySelectorAll('.header__slide-img');
    let countSlides = slides.length;
    
    if(countSlides > 0){
        setInterval( () => {
            slides.forEach( (slide, number) => {
                if(counter == number){
                    slide.classList.add("header__slide-active");
                } else {
                    slide.classList.remove("header__slide-active");
                }
    
            })
            counter++;
            counter = counter == countSlides ? 0 : counter;
        }, timeSlide);
    }
}

sliding();